let http = require("http")

const server = http.createServer(function(request, response){

	if(request.url === '/api/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Users retrieved from database.")

	}else if(request.url === '/api/users/3141592654' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User profile retrieved from database.")

	}else if(request.url === '/api/users/3141592654' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User's information has been updated.")	

	}else if(request.url === '/api/users/3141592654' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User deleted successfully.")	
			
	}else if(request.url === '/api/users' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("New user registered successfully!")
				
	}
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)